datasets = "configs/2016/datasets/tWScaleSysts.txt";
cuts = "configs/2016/cuts/ttbarCuts.txt";
plots = "configs/plots/plotTtbarConf.cfg";
outputFolder = "plots/2016/tW/";
outputPostfix = "ee";
channelName = "ee";
