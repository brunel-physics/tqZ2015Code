datasets = "configs/2016/datasets/tZqScaleSysts.txt";
cuts = "configs/2016/cuts/ZplusCuts.txt";
plots = "configs/plots/plotDileptonConf.cfg";
outputFolder = "plots/2016/tZq/";
outputPostfix = "ee";
channelName = "ee";
