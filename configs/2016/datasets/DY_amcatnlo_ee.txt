[DYJetsToLL_M-50_amcatnlo]
fileName = configs/2016/datasets/fileLists/DYJets50Files.txt
totalEvents = 122055388
crossSection = 5941.0
runType = mc
histoName = zPlusJetsAmcAtNlo
colour = kBlue
label = zPlusJets
plotType = f

[DYJetsToLL_M-10To50_amcatnlo]
fileName = configs/2016/datasets/fileLists/DYJets10To50Files.txt
totalEvents = 106269624
crossSection = 18810.0
runType = mc
histoName = zPlusJetsAmcAtNlo
colour = kBlue
label = zPlusJets
plotType = f

[ttbarInclusivePowerheg]
fileName = configs/2016/datasets/fileLists/ttbarInclusivePowerhegFiles.txt
totalEvents = 77081156
crossSection = 831.76
runType = mc
histoName = ttbarInc
colour = kRed
label = ttbar
plotType = f

[tHq]
fileName = configs/2016/datasets/fileLists/tHqFiles.txt
totalEvents = 3495799
crossSection = 0.07462
runType = mc
histoName = SingleTop
colour = kMagenta
label = Single Top
plotType = f

[sChannel]
fileName = configs/2016/datasets/fileLists/sChannelFiles.txt
totalEvents = 2989199
crossSection = 10.32
runType = mc
histoName = SingleTop
colour = kMagenta
label = Single Top
plotType = f

[tChannel]
fileName = configs/2016/datasets/fileLists/tChannelFiles.txt
totalEvents = 67240808
crossSection = 136.02
runType = mc
histoName = SingleTop
colour = kMagenta
label = Single Top
plotType = f

[tbarChannel]
fileName = configs/2016/datasets/fileLists/tbarChannelFiles.txt
totalEvents = 38811017
crossSection = 80.95
runType = mc
histoName = SingleTop
colour = kMagenta
label = Single Top
plotType = f

[WZ1l1nu2q]
fileName = configs/2016/datasets/fileLists/WZ1l1nu2q.txt
totalEvents = 24221923
crossSection = 10.73
runType = mc
histoName = wz
colour = kOrange
label = wz
plotType = f

[WZ2l2q]
fileName = configs/2016/datasets/fileLists/WZ2l2q.txt
totalEvents = 26517272
crossSection = 5.606
runType = mc
histoName = wz
colour = kOrange
label = wz
plotType = f

[WZjets]
fileName = configs/2016/datasets/fileLists/WZjets.txt
totalEvents = 1930828
crossSection = 5.26
runType = mc
histoName = wz
colour = kOrange
label = wz
plotType = f

[ZZ4l]
fileName = configs/2016/datasets/fileLists/ZZ4lFiles.txt
totalEvents = 10709784
crossSection = 1.204
runType = mc
histoName = zz
colour = kPink
label = zz
plotType = f

[ZZ2l2nu]
fileName = configs/2016/datasets/fileLists/ZZ2l2nuFiles.txt
totalEvents = 8842475
crossSection = 0.5644
histoName = zz
colour = kPink
label = zz
plotType = f
runType = mc

[ZZ2l2q]
fileName = configs/2016/datasets/fileLists/ZZ2l2qFiles.txt
runType = mc
crossSection = 3.222
totalEvents = 15345572
histoName = zz
colour = kPink
label = zz
plotType = f

[WW1l1nu2q]
fileName = configs/2016/datasets/fileLists/WW1l1nu2qFiles.txt
totalEvents = 8997800
crossSection = 49.997
runType = mc
histoName = WW
colour = kCyan
label = WW
plotType = f

[WW2l2nu]
fileName = configs/2016/datasets/fileLists/WW2l2nuFiles.txt
totalEvents = 1999000
crossSection = 12.178
runType = mc
histoName = WW
colour = kCyan
label = WW
plotType = f

[WWW]
fileName = configs/2016/datasets/fileLists/WWWFiles.txt
totalEvents = 240000
crossSection = 0.2086
runType = mc
histoName = vvv
colour = kViolet
label = vvv
plotType = f

[WWZ]
fileName = configs/2016/datasets/fileLists/WWZFiles.txt
totalEvents = 250000
crossSection = 0.1651
runType = mc
histoName = vvv
colour = kViolet
label = vvv
plotType = f

[WZZ]
fileName = configs/2016/datasets/fileLists/WZZFiles.txt
totalEvents = 246800
crossSection = 0.05565
runType = mc
histoName = vvv
colour = kViolet
label = vvv
plotType = f

[ZZZ]
fileName = configs/2016/datasets/fileLists/ZZZFiles.txt
totalEvents = 249237
crossSection = 0.01398
runType = mc
histoName = vvv
colour = kViolet
label = vvv
plotType = f

[wPlusJets]
fileName = configs/2016/datasets/fileLists/wPlusJetsFiles.txt
runType = mc
totalEvents = 259604315
crossSection = 61526.7
histoName = wJets
colour = kGray
label = w+jets
plotType = f

[ttWlnu]
fileName = configs/2016/datasets/fileLists/ttWlnuFiles.txt
totalEvents = 5280565
crossSection = 0.2001
runType = mc
histoName = ttV
colour = kGreen
label = ttV
plotType = f

[ttW2q]
fileName = configs/2016/datasets/fileLists/ttW2qFiles.txt
totalEvents = 833298
crossSection = 0.405
runType = mc
histoName = ttV
colour = kGreen
label = ttV
plotType = f

[ttZ2l2nu]
fileName = configs/2016/datasets/fileLists/ttZ2l2nuFiles.txt
totalEvents = 13908701
crossSection = 0.2529
runType = mc
histoName = ttV
colour = kGreen
label = ttV
plotType = f

[ttZ2q]
fileName = configs/2016/datasets/fileLists/ttZ2qFiles.txt
totalEvents = 749400
crossSection = 0.5297
runType = mc
histoName = ttV
colour = kGreen
label = ttV
plotType = f

[ttHTobb]
fileName = configs/2016/datasets/fileLists/ttHTobbFiles.txt
totalEvents = 3845992
crossSection = 0.2942
runType = mc
histoName = ttV
colour = kGreen
label = ttV
plotType = f

[ttHToNonbb]
fileName = configs/2016/datasets/fileLists/ttHToNonbbFiles.txt
totalEvents = 3981250
crossSection = 0.2123
runType = mc
histoName = ttV
colour = kGreen
label = ttV
plotType = f

[tWInclusive]
fileName = configs/2016/datasets/fileLists/tWInclusiveFiles.txt
totalEvents = 6952830
crossSection = 35.85
runType = mc
histoName = SingleTop
colour = kMagenta
label = Single Top
plotType = f

[tbarWInclusive]
fileName = configs/2016/datasets/fileLists/tbarWInclusiveFiles.txt
totalEvents = 6933094
crossSection = 35.85
runType = mc
histoName = SingleTop
colour = kMagenta
label = Single Top
plotType = f

[tWZ]
fileName = configs/2016/datasets/fileLists/tWZ_ll_Files.txt
totalEvents = 50000
crossSection = 0.01104
runType = mc
histoName = SingleTop
colour = kMagenta
label = Single Top
plotType = f

[tZq]
fileName = configs/2016/datasets/fileLists/tZqFiles.txt
totalEvents = 14509520
crossSection = 0.0758
runType = mc
histoName = tZq
colour = kYellow
label = tZq
plotType = f

[eeRun2016]
fileName = configs/2016/datasets/fileLists/eeRun2016Files.txt
luminosity = 35858.984
runType = data
histoName = data
label = Data
plotType = p
colour = kBlack
triggerFlag = eCt

[]

